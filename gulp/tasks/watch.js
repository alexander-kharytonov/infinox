import gulp from 'gulp';
import connect from 'gulp-connect';

import config from '../config';

gulp.task('watch:styles', () => {
  gulp.watch(config.styles.src + '**/*.{css,less}', ['styles']);
});
gulp.task('watch:scripts', () => {
  gulp.watch(config.scripts.src + '**/*.js', ['scripts']);
});
gulp.task('watch:html', () => {
  gulp.watch(config.webroot + '**/*.html', ['html']);
});
gulp.task('watch', 'Watching task', [
  'watch:styles',
  'watch:scripts',
  'watch:html'
]);

gulp.task('html', () => {
  return gulp.src(config.webroot)
    .pipe(connect.reload());
});
