import gulp from 'gulp';
import gutil from 'gulp-util';
import gulpif from 'gulp-if';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import sourcemaps from 'gulp-sourcemaps';
import connect from 'gulp-connect';

import config from '../config';

const NODE_ENV = process.env.NODE_ENV || 'production';

const scriptsConfig = {
  core: {
    name: 'core.js',
    src: [
      config.modules + 'jquery/dist/jquery.js',
      config.modules + 'aos/dist/aos.js',
      config.modules + 'owl.carousel/dist/owl.carousel.js',
      config.modules + 'jarallax/dist/jarallax.min.js',
      config.modules + 'jarallax/dist/jarallax-video.min.js',
      config.scripts.src + 'custom.js'
    ],
    dist: config.scripts.dist
  }
};

gulp.task('scripts:core', compileScripts(scriptsConfig.core));
gulp.task('scripts', 'Rebuild scripts', [
  'scripts:core'
]);

function compileScripts(config) {
  return () => {
    return gulp.src(config.src)
      .pipe(gulpif(NODE_ENV == 'development',
        sourcemaps.init()
      ))
      .pipe(concat(config.name))
      .pipe(gulpif(NODE_ENV == 'production',
        uglify()
      ))
      .on('error', function (error) {
        gutil.log(gutil.colors.red(error.message));
        this.emit('end');
      })
      .pipe(gulpif(NODE_ENV == 'development',
        sourcemaps.write('./maps')
      ))
      .pipe(gulp.dest(config.dist))
      .pipe(connect.reload())
      .on('error', gutil.log);
  }
}
