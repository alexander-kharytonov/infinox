import gulp from 'gulp';
import config from '../config';
import gutil from 'gulp-util';
import gulpif from 'gulp-if';
import base64 from 'gulp-base64';
import less from 'gulp-less';
import concat from 'gulp-concat';
import sourcemaps from 'gulp-sourcemaps';
import cleanCSS from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import connect from 'gulp-connect';

const NODE_ENV = process.env.NODE_ENV || 'production';

const stylesConfig = {
  global: {
    name: 'app.css',
    src: [
      config.modules + 'normalize.css/normalize.css',
      config.modules + 'aos/dist/aos.css',
      config.ico.dist + 'bundle/ico.less',
      config.styles.src + 'app.less'
    ],
    dist: config.styles.dist
  }
};

gulp.task('styles:global', ['webfont'], compileStyles(stylesConfig.global));
gulp.task('styles', 'Rebuild stylesheets', [
  'styles:global'
]);

function compileStyles(config) {
  return () => {
    return gulp.src(config.src)
      .pipe(gulpif(NODE_ENV == 'development',
        sourcemaps.init()
      ))
      .pipe(less({
        compress: false
      }))
      .on('error', function (error) {
        gutil.log(gutil.colors.red(error.message));
        this.emit('end');
      })
      .pipe(base64({
        extensions: ['svg', 'eot', 'woff2', 'woff', 'ttf']
      }))
      .pipe(autoprefixer({
        browsers: ['last 2 versions']
      }))
      .pipe(concat(config.name))
      .pipe(cleanCSS())
      .pipe(gulpif(NODE_ENV == 'development',
        sourcemaps.write('./maps')
      ))
      .pipe(gulp.dest(config.dist))
      .pipe(connect.reload())
      .on('error', gutil.log);
  }
}
